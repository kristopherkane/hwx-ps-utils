Cluster Administration

pdsh - Distributed command execution using SSH - assumes shared SSH keys

>yum install pdsh

Run the date command on hosts host-01 to host-10
>pdsh -w host-[01-10] "date"

If all cluster server hostnames are available in /etc/hosts:

>for i in `awk '{ print $2 }' /etc/hosts | grep "host-" |  grep -v $HOSTNAME`; do ssh $i "date"; done
